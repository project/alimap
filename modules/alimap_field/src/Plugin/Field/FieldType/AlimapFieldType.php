<?php
namespace Drupal\alimap_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of alimap_field.
 *
 * @FieldType(
 *   id = "alimap_field",
 *   label = @Translation("Alimap field"),
 *   default_formatter = "alimap_field_default",
 *   default_widget = "alimap_field_default",
 * )
 */
class AlimapFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'lat' => [
          'type' => 'float',
          'size' => 'big',
          'default' => 0.0,
          'not null' => FALSE,
        ],
        'lon' => [
          'type' => 'float',
          'size' => 'big',
          'default' => 0.0,
          'not null' => FALSE,
        ],
        'infowindow' => [
          'type' => 'text',
          'size' => 'medium',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['lat'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Latitude'));

    $properties['lon'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Longitude'));

    $properties['infowindow'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('InfoWindow message'));

    return $properties;
  }

}
