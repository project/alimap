(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.geofieldBaiduMap = {
    attach: function (context, drupalSettings) {
      if (drupalSettings['alimap']) {
        $(context).find('.alimap-container').once().each(function (index, element) {
          let mapid = $(element).attr('id');
          let settings = drupalSettings.alimap[mapid];
          window._AMapSecurityConfig = {
            securityJsCode: settings['security_key'],
          };

          // start amap render.
          AMapLoader.load({
            key: settings['api_key'],
            version: "2.0",
          })
          .then((AMap) => {

            let map_config = {
              viewMode: '2D',
              resizeEnable: true,
              // center: [settings['lon'], settings['lat']],
              zoom: settings['map_style']['zoom'],
            };

            //地图加载
            var map = new AMap.Map(settings['mapid'], map_config);
            var overlays = [];

            var parseData = function(type, data) {
              var newobj = JSON.parse(data);
              switch(type) {
                case 'polygon': {
                  var polygon = new AMap.Polygon({
                    path: newobj
                  });
                  overlays.push(polygon);
                  map.add(polygon);
                  break;
                }
                case 'polyline': {
                  var polyline = new AMap.Polyline({
                    path: newobj,
                  });
                  overlays.push(polyline);
                  map.add(polyline);
                  break;
                }

                case 'rectangle': {
                  let southWest = newobj['northEast'];
                  let northEast = newobj['southWest'];
                  var bounds = new AMap.Bounds(southWest, northEast);
                  var rectangle = new AMap.Rectangle({
                    bounds: bounds
                  });
                  overlays.push(rectangle);
                  map.add(rectangle);
                  break;
                }

                case 'circle': {
                  let circle = new AMap.Circle({
                    center: newobj['center'],
                    radius: newobj['radius']
                  });
                  overlays.push(circle);
                  map.add(circle);
                  break;
                }
              }
            }

            if (settings['type'] && settings['geodata']) {
              parseData(settings['type'], settings['geodata'])
            }

            if (settings['data']) {
              settings['data'].forEach(function(item) {
                parseData(item['type'], item['geodata']);
              });
            }

            var writeGeoData = function(data) {
              $("#"+settings['geodata_id']).val(data);
            }
            AMap.plugin('AMap.MouseTool',function(){
              if (!settings['map_controls']['placesearch']) {
                return;
              }
              var mouseTool = new AMap.MouseTool(map);
              var draw = function(type) {
                switch(type){
                  case 'polyline':{
                    mouseTool.polyline({
                      strokeColor:'#80d8ff'
                      //同Polyline的Option设置
                    });
                    break;
                  }
                  case 'polygon':{
                    mouseTool.polygon({
                      fillColor:'#00b0ff',
                      strokeColor:'#80d8ff'
                      //同Polygon的Option设置
                    });
                    break;
                  }
                  case 'rectangle':{
                    mouseTool.rectangle({
                      fillColor:'#00b0ff',
                      strokeColor:'#80d8ff'
                      //同Polygon的Option设置
                    });
                    break;
                  }
                  case 'circle':{
                    mouseTool.circle({
                      fillColor:'#00b0ff',
                      strokeColor:'#80d8ff'
                      //同Circle的Option设置
                    });
                    break;
                  }
                }
              }
              mouseTool.on('draw',function(e){
                overlays.push(e.obj);

                // 折线就用json.
                if (e.obj.CLASS_NAME == 'Overlay.Polyline') {
                  let data = e.obj.getPath().map((ele) => [ele.lng, ele.lat]);
                  let json = JSON.stringify(data);
                  writeGeoData(json);
                }

                if (e.obj.CLASS_NAME == 'Overlay.Polygon') {
                  let data = e.obj.getPath().map((ele) => [ele.lng, ele.lat]);
                  let json = JSON.stringify(data);
                  writeGeoData(json);
                }

                if (e.obj.CLASS_NAME == 'Overlay.Rectangle') {
                  let bounds = e.obj.getBounds();

                  let data = {
                    northEast: [bounds.northEast.lng, bounds.northEast.lat],
                    southWest: [bounds.southWest.lng, bounds.southWest.lat]
                  };
                  let json = JSON.stringify(data);
                  writeGeoData(json);
                }

                if (e.obj.CLASS_NAME == 'Overlay.Circle') {
                  let data = {
                    center: [e.obj.getCenter().lng,e.obj.getCenter().lat],
                    radius: e.obj.getRadius()
                  }
                  let json = JSON.stringify(data);
                  writeGeoData(json);
                }
              });
              if (settings['type']) {
                draw(settings['type']);
              }
              $("#"+settings['alimap_polygon_type_id']+" .alimap_polygon_type.form-radio").on("change", function() {
                draw($(this).val());
                writeGeoData('');
                map.remove(overlays);
                overlays = [];
              });
            })

            // 设置地图样式.
            map.setMapStyle('amap://styles/' + settings['map_style']['style']);

            if (settings['map_controls']['placesearch']) {
              // 地图点击事件.
              // map.on('click', function(e) {
              //   map.clearMap();
              //   createMark(e.lnglat.lng, e.lnglat.lat);
              //   $('#'+settings['latid']).val(e.lnglat.lat);
              //   $('#'+settings['lonid']).val(e.lnglat.lng);
              // });

              AMap.plugin(['AMap.PlaceSearch', 'AMap.AutoComplete'], function () {
                var auto = new AMap.AutoComplete({
                  input: settings['tipinput_id'],
                  city: '全国'
                });
                var placeSearch = new AMap.PlaceSearch({
                  map: map
                });  //构造地点查询类

                //注册监听，当选中某条记录时会触发
                auto.on("select", function(e) {
                  $('#'+settings['latid']).val(e.poi.location.lat);
                  $('#'+settings['lonid']).val(e.poi.location.lng);
                  placeSearch.setCity(e.poi.adcode);
                  placeSearch.search(e.poi.name);  //关键字查询查询
                });
              });
            }

            if (settings['map_controls']['toolbar']) {
              AMap.plugin('AMap.ToolBar',function(){
                let control = new AMap.ToolBar();
                map.addControl(control);
              });
            }

            if (settings['map_controls']['scale']) {
              AMap.plugin('AMap.Scale',function(){
                let control = new AMap.Scale();
                map.addControl(control);
              });
            }

            if (settings['map_controls']['controlbar']) {
              AMap.plugin('AMap.ControlBar',function(){
                let control = new AMap.ControlBar();
                map.addControl(control);
              });
            }

            if (settings['map_controls']['geolocation']) {
              AMap.plugin('AMap.Geolocation',function(){
                let control = new AMap.Geolocation();
                map.addControl(control);
              });
            }

            if (settings['map_controls']['hawkeye']) {
              AMap.plugin('AMap.HawkEye',function(){
                let control = new AMap.HawkEye();
                map.addControl(control);
              });
            }

            if (settings['map_controls']['maptype']) {
              AMap.plugin('AMap.MapType',function(){
                var control = new AMap.MapType();
                map.addControl(control);
              });
            }

            map.setFitView();
          })
          .catch((e) => {
            console.error(e); //加载错误提示
          });
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);
