<?php
namespace Drupal\alimap_field\Plugin\Field\FieldFormatter;

use Drupal\alimap\AlimapTrait;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'google_map_field' formatter.
 *
 * @FieldFormatter(
 *   id = "alimap_field_default",
 *   label = @Translation("Alimap field default"),
 *   field_types = {
 *     "alimap_field"
 *   }
 * )
 */
class AlimapFieldDefaultFormatter extends FormatterBase {
  use AlimapTrait;
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'map_style' => [
          'zoom' => 9,
          'maptype' => 0,
          'style' => 'normal'
        ],
        'map_dimensions' => [
          'width' => '100%',
          'height' => '450px',
        ],
        'map_controls' => [
          'maptype' => false,
          'toolbar' => false,
          'scale' => false,
          'placesearch' => false,
          'controlbar' => false,
          'geolocation' => false,
          'hawkeye' => false,
        ]
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    $settings = $this->getSettings();
    $elements['#tree'] = TRUE;
    $this->setMapDimensionsElement($settings, $elements);
    $this->setMapStyle($settings, $elements);
    return $elements;
  }

  public function settingsSummary() {
    foreach ($this->getDefaultSettings() as $group_key => $group) {
      if ($group_key == 'map_controls') {
        continue;
      }
      foreach ($group as $key => $item) {
        $summary[$group_key][$key] = [
          '#markup' => '<br />' . $this->t("@title: @key_value", [
              '@title' => ucfirst($key),
              '@key_value' => $this->getSetting($group_key)[$key] ?? ''
            ])
        ];
      }
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();
    foreach ($items as $delta => $item) {
      $mapid = 'alimap-container-' . $delta;
      $lat_id = 'alimap-lat-' . $delta;
      $lon_id = 'alimap-lon-' . $delta;
      $element['map'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'id' => $mapid,
          'class' => 'alimap-container',
          'style' => "width: {$settings['map_dimensions']['width']};height: {$settings['map_dimensions']['height']};"
        ],
      ];

      // Geofield Map Element specific mapid settings.
      $map_settings = [
          'api_key' => $this->getAlimapApiKey(),
          'security_key' => $this->getAlimapSecurityKey(),
          'mapid' => $mapid,
          'latid' => $lat_id,
          'lonid' => $lon_id,
          'lat' => $items[$delta]->lat,
          'lon' => $items[$delta]->lon,
          'infowindow' => $items[$delta]->infowindow,
        ] + $settings;

      $settings[$mapid] = $map_settings;
      $element['#attached']['drupalSettings'] = [
        'alimap' => $settings,
      ];

      $elements[$delta] = $element;
    }

    $elements['#attached']['library'][] = 'alimap/alimap_loader';
    $elements['#attached']['library'][] = 'alimap_field/alimap_field_widget';

    return $elements;
  }

}
