<?php

namespace Drupal\alimap;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;

trait AlimapTrait {
  use StringTranslationTrait;

  public function getAlimapSettings() {
    $settings = \Drupal::configFactory()->get('alimap.settings');
    return [
      'api_key' => $settings->get('alimap_api_key'),
      'security_key' => $settings->get('alimap_security_key')
    ];
  }

  public function getAlimapApiKey() {
    $settings = $this->getAlimapSettings();
    return $settings['api_key'];
  }

  public function getAlimapSecurityKey() {
    $settings = $this->getAlimapSettings();
    return $settings['security_key'];
  }

  /**
   * Get the Default Settings.
   *
   * @return array
   *   The default settings.
   */
  public function getDefaultSettings() {
    return [
      'map_style' => [
        'zoom' => 9,
        'maptype' => 0,
        'style' => 'normal'
      ],
      'map_dimensions' => [
        'width' => '100%',
        'height' => '450px',
      ],
      'map_controls' => [
        'maptype' => false,
        'toolbar' => true,
        'placesearch' => true,
        'scale' => true,
        'controlbar' => true,
        'geolocation' => true,
        'hawkeye' => true,
      ]
    ];
  }

  /**
   * Set Map Dimension Element.
   *
   * @param array $settings
   *   The Form Settings.
   * @param array $elements
   *   The Form element to alter.
   */
  public function setMapDimensionsElement(array $settings, array &$elements) {
    $elements['map_dimensions'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Map Dimensions'),
    ];
    $elements['map_dimensions']['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Map width'),
      '#default_value' => $settings['map_dimensions']['width'],
      '#size' => 25,
      '#maxlength' => 25,
      '#description' => $this->t('The default width of a Baidu map, as a CSS length or percentage. Examples: <em>50px</em>, <em>5em</em>, <em>2.5in</em>, <em>95%</em>.'),
      '#required' => TRUE,
    ];
    $elements['map_dimensions']['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Map height'),
      '#default_value' => $settings['map_dimensions']['height'],
      '#size' => 25,
      '#maxlength' => 25,
      '#description' => $this->t('The default height of a Baidu map, as a CSS length or percentage. Examples: <em>50px</em>, <em>5em</em>, <em>2.5in</em>, <em>95%</em>'),
      '#required' => TRUE,
    ];
    return $elements;
  }

  public function setMapStyle(array $settings, array &$elements) {
    $elements['map_style'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Map Style'),
    ];
    $options = array_combine(range(1, 18), range(1,18));
    $options['auto'] = $this->t('Automatic');

    $elements['map_style']['zoom'] = array(
      '#type' => 'select',
      '#title' => $this->t('Zoom'),
      '#default_value' => $settings['map_style']['zoom'] ?? $this->getDefaultSettings()['map_style']['zoom'],
      // drupal_map_assoc(range(1, 18))
      '#options' => $options,
      '#description' => $this->t('The default zoom level of a Baidu map, ranging from 0 to 20 (the greatest). Select <em>Automatic</em> for the map to automatically center and zoom to show all locations.'),
    );
    $elements['map_style']['maptype'] = [
      '#type' => 'select',
      '#title' => $this->t('Map type'),
      '#default_value' => $settings['map_style']['maptype'] ?? $this->getDefaultSettings()['map_style']['maptype'],
      '#options' => [
        0 => $this->t('Standard'),
        1 => $this->t('Satellite')
      ],
    ];

    $elements['map_style']['style'] = [
      '#type' => 'select',
      '#title' => $this->t('Map Style'),
      '#default_value' => $settings['map_style']['style'] ?? $this->getDefaultSettings()['map_style']['style'],
      '#options' => [
        'normal' => $this->t('Normal 标准'),
        'dark' => $this->t('(Dark) 幻影黑'),
        'light' => $this->t('(Light) 月光银'),
        'whitesmoke' => $this->t('(Whitesmoke) 远山黛'),
        'fresh' => $this->t('(Fresh) 草色青'),
        'grey' => $this->t('(Grey) 雅士灰'),
        'graffiti' => $this->t('(Graffiti) 涂鸦'),
        'macaron' => $this->t('(Macaron) 马卡龙'),
        'blue' => $this->t('(Blue) 靛青蓝'),
        'darkblue' => $this->t('(Darkblue) 极夜蓝'),
        'wine' => $this->t('(Wine) 酱籽'),
      ]
    ];
    return $elements;
  }

  /**
   * Set Map Controls and Pan Element.
   *
   * @param array $settings
   *   The Form Settings.
   * @param array $elements
   *   The Form element to alter.
   */
  public function setMapControls(array $settings, array &$elements) {
    $elements['map_controls'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Map Controls'),
    ];

    foreach($this->getDefaultSettings()['map_controls'] as $control => $default_value) {
      $elements['map_controls'][$control] = [
        '#type' => 'checkbox',
        '#title' => $this->t(ucfirst($control)),
        '#default_value' => $settings['map_controls'][$control] ?? $default_value,
      ];
    }
    return $elements;
  }
}
