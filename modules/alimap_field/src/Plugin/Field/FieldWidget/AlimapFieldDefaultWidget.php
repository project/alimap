<?php
namespace Drupal\alimap_field\Plugin\Field\FieldWidget;

use Drupal\alimap\AlimapTrait;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'google_map_field_default' widget.
 *
 * @FieldWidget(
 *   id = "alimap_field_default",
 *   label = @Translation("Alimap field default"),
 *   field_types = {
 *     "alimap_field"
 *   }
 * )
 */
class AlimapFieldDefaultWidget extends WidgetBase {
  use AlimapTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
   return [
       'map_style' => [
         'zoom' => 9,
         'maptype' => 0,
         'style' => 'normal'
       ],
       'map_dimensions' => [
         'width' => '100%',
         'height' => '450px',
       ],
       'map_controls' => [
         'maptype' => false,
         'toolbar' => true,
         'placesearch' => true,
         'scale' => true,
         'controlbar' => true,
         'geolocation' => true,
         'hawkeye' => true,
       ]
     ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    $settings = $this->getSettings();
    $elements['#tree'] = TRUE;
    $this->setMapDimensionsElement($settings, $elements);
    $this->setMapControls($settings, $elements);
    $this->setMapStyle($settings, $elements);
    return $elements;
  }

  public function settingsSummary() {
    $api_key = $this->getAlimapApiKey();

    // Define the Baidu Maps API Key value message string.
    if (!empty($api_key)) {
      $state = \Drupal::linkGenerator()->generate($api_key, Url::fromRoute('alimap.settings', [], [
        'query' => [
          'destination' => Url::fromRoute('<current>')
            ->toString(),
        ],
      ]));
    }
    else {
      $state = t("<span class='geofield-baidu-map-warning'>Alimap Api Key missing<br>Geocode functionalities not available.</span> @settings_page_link", [
        '@settings_page_link' => \Drupal::linkGenerator()->generate(t('Set it in the Alimap Configuration Page'), Url::fromRoute('alimap.settings', [], [
          'query' => [
            'destination' => Url::fromRoute('<current>')
              ->toString(),
          ],
        ])),
      ]);
    }

    $summary['api_key'] = [
      '#markup' => $this->t('Alimap API Key: @state', [
        '@state' => $state,
      ])
    ];

    foreach ($this->getDefaultSettings() as $group_key => $group) {
      foreach ($group as $key => $item) {
        $summary[$group_key][$key] = [
          '#markup' => '<br />' . $this->t("@title: @key_value", [
            '@title' => ucfirst($key),
            '@key_value' => $this->getSetting($group_key)[$key] ?? ''
          ])
        ];
      }
    }

    return $summary;
  }

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $mapid = 'alimap-container-' . $delta;
    $settings = $this->getSettings();
    $alimap_tipinput_id = 'alimap_tipinput_' . $delta;
    $lat_id = 'alimap-lat-' . $delta;
    $lon_id = 'alimap-lon-' . $delta;

    if ($settings['map_controls']['placesearch']) {
      $element['placesearch'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Place search'),
        '#attributes' => [
          'id' => $alimap_tipinput_id
        ]
      ];
    }
    $element['map'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'id' => $mapid,
        'class' => 'alimap-container',
        'style' => "width: {$settings['map_dimensions']['width']};height: {$settings['map_dimensions']['height']};"
      ],
    ];

    $element['lat'] = [
      '#title' => $this->t('Latitude'),
      '#type' => 'textfield',
      '#size' => 18,
      '#attributes' => [
        'id' => $lat_id
      ],
      '#default_value' => $items[$delta]->lat ?? NULL,
    ];

    $element['lon'] = [
      '#title' => $this->t('Longitude'),
      '#type' => 'textfield',
      '#size' => 18,
      '#attributes' => [
        'id' => $lon_id
      ],
      '#default_value' => $items[$delta]->lon ?? NULL,
    ];

    $element['infowindow'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Info window'),
      '#default_value' => $items[$delta]->infowindow ?? "",
    ];

    $element['#attached']['library'][] = 'alimap/alimap_loader';
    $element['#attached']['library'][] = 'alimap_field/alimap_field_widget';

    // Geofield Map Element specific mapid settings.
    $map_settings = [
        'api_key' => $this->getAlimapApiKey(),
        'security_key' => $this->getAlimapSecurityKey(),
        'mapid' => $mapid,
        'latid' => $lat_id,
        'lonid' => $lon_id,
        'lat' => $items[$delta]->lat,
        'lon' => $items[$delta]->lon,
        'infowindow' => $items[$delta]->infowindow,
        'tipinput_id' => $alimap_tipinput_id
    ] + $settings;

    $settings[$mapid] = $map_settings;

    $element['#attached']['drupalSettings'] = [
      'alimap' => $settings,
    ];
    return $element;
  }
}
