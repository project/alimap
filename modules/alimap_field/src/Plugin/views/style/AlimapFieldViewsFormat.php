<?php
namespace Drupal\alimap_field\Plugin\views\style;

use Drupal\alimap\AlimapTrait;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\core\form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render a list of years and months
 * in reverse chronological order linked to content.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "alimap_field",
 *   title = @Translation("Alimap Field"),
 *   help = @Translation("Alimap Field."),
 *   display_types = { "normal" }
 * )
 */
class AlimapFieldViewsFormat extends StylePluginBase {
  use AlimapTrait;
  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['data_source'] = ['default' => ''];
    // See: baidu_map_geofield_field_formatter_info.
    $options['map_dimensions'] = [
      'default' => [
        'width' => '100%',
        'height' => '400px'
      ]
    ];
    $options['map_style'] = ['default' => [
      'zoom' => 9,
      'maptype' => 0,
      'style' => 'normal'
    ]];
    $options['map_controls'] = [
      'default' => [
        'maptype' => false,
        'toolbar' => false,
        'placesearch' => false,
        'scale' => false,
        'controlbar' => false,
        'geolocation' => false,
        'hawkeye' => false,
      ]
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    if (!$this->usesFields() && $this->usesGrouping()) {
      $form['error'] = array(
        '#markup' => t('Please add at least 1 geofield to the view'),
      );
    } else {
      $options = ['' => $this->t('- None -')];
      $field_labels = $this->displayHandler->getFieldLabels(TRUE);
      $options += $field_labels;

      $form['data_source'] = array(
        '#type' => 'select',
        '#title' => t('Data Source'),
        '#description' => t('Which field contains geodata?'),
        '#options' => $options,
        '#default_value' => $this->options['data_source'] ? $this->options['data_source'] : '',
      );
      $this->setMapDimensionsElement($this->options, $form);
      $this->setMapStyle($this->options, $form);
    }
    return $form;
  }

  public function render() {
    $souce_field = $this->options['data_source'];
    $values = $this->view->result;

    $geodata = [];
    foreach ($values as $index => $value) {
      if (!$value) {
        continue;
      }
      foreach ($value->_entity->get($souce_field)->getValue() as $item) {
        $geodata[] = $item;
      }
    }
    $container_id = $this->view->id() . '_' . $this->view->current_display;
    $js_settings = array(
      $container_id => $this->options + array(
        'mapid' => $container_id,
        'api_key' => $this->getAlimapApiKey(),
        'security_key' => $this->getAlimapSecurityKey(),
        'map_settings' => $this->options,
        'data' => $geodata,
      ),
    );
    $width = $this->options['map_dimensions']['width'];
    $height = $this->options['map_dimensions']['height'];
    $display['map'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'id' => $container_id,
        'class' => 'alimap-container',
        'style' => "width: {$width};height: {$height};"
      ],
    ];

    $display['#attached']['library'][] = 'alimap/alimap_loader';
    $display['#attached']['library'][] = 'alimap_field/alimap_field_widget';

    $display['#attached']['drupalSettings'] = [
      'alimap' => $js_settings,
    ];
    return $display;
  }
}
