(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.geofieldBaiduMap = {
    attach: function (context, drupalSettings) {
      if (drupalSettings['alimap']) {
        $(context).find('.alimap-container').once().each(function (index, element) {
          let mapid = $(element).attr('id');
          let settings = drupalSettings.alimap[mapid];
          window._AMapSecurityConfig = {
            securityJsCode: settings['security_key'],
          };

          // start amap render.
          AMapLoader.load({
            key: settings['api_key'],
            version: "2.0",
          })
            .then((AMap) => {

              let map_config = {
                viewMode: '2D',
                resizeEnable: true,
                // center: [settings['lon'], settings['lat']],
                zoom: settings['map_style']['zoom'],
              };

              //地图加载
              var map = new AMap.Map(settings['mapid'], map_config);

              var createMark = function(lon, lat, infowindow = '') {
                const position = new AMap.LngLat(lon, lat);
                const marker = new AMap.Marker({
                  position: position,
                  title: infowindow
                });
                if (infowindow) {
                  let infoWindow = new AMap.InfoWindow({
                    content: infowindow,
                    offset: [0, -35]
                  });
                  marker.on('click', function() {
                    infoWindow.open(map, position);
                  });
                }
                map.add(marker);
              }

              if (settings['lon'] && settings['lat']) {
                createMark(settings['lon'], settings['lat'], settings['infowindow'])
              }

              // each multiple data.
              if (settings['data']) {
                settings['data'].forEach(function(item) {
                  createMark(item['lon'], item['lat'], item['infowindow'])
                })
              }

              // 设置地图样式.
              map.setMapStyle('amap://styles/' + settings['map_style']['style']);

              if (settings['map_controls']['placesearch']) {
                // 地图点击事件.
                map.on('click', function(e) {
                  map.clearMap();
                  createMark(e.lnglat.lng, e.lnglat.lat);
                  $('#'+settings['latid']).val(e.lnglat.lat);
                  $('#'+settings['lonid']).val(e.lnglat.lng);
                });

                AMap.plugin(['AMap.PlaceSearch', 'AMap.AutoComplete'], function () {
                  var auto = new AMap.AutoComplete({
                    input: settings['tipinput_id'],
                    city: '全国'
                  });
                  var placeSearch = new AMap.PlaceSearch({
                    map: map
                  });  //构造地点查询类

                  //注册监听，当选中某条记录时会触发
                  auto.on("select", function(e) {
                    $('#'+settings['latid']).val(e.poi.location.lat);
                    $('#'+settings['lonid']).val(e.poi.location.lng);
                    placeSearch.setCity(e.poi.adcode);
                    placeSearch.search(e.poi.name);  //关键字查询查询
                  });
                });
              }

              if (settings['map_controls']['toolbar']) {
                AMap.plugin('AMap.ToolBar',function(){
                  let control = new AMap.ToolBar();
                  map.addControl(control);
                });
              }

              if (settings['map_controls']['scale']) {
                AMap.plugin('AMap.Scale',function(){
                  let control = new AMap.Scale();
                  map.addControl(control);
                });
              }

              if (settings['map_controls']['controlbar']) {
                AMap.plugin('AMap.ControlBar',function(){
                  let control = new AMap.ControlBar();
                  map.addControl(control);
                });
              }

              if (settings['map_controls']['geolocation']) {
                AMap.plugin('AMap.Geolocation',function(){
                  let control = new AMap.Geolocation();
                  map.addControl(control);
                });
              }

              if (settings['map_controls']['hawkeye']) {
                AMap.plugin('AMap.HawkEye',function(){
                  let control = new AMap.HawkEye();
                  map.addControl(control);
                });
              }

              if (settings['map_controls']['maptype']) {
                AMap.plugin('AMap.MapType',function(){
                  var control = new AMap.MapType();
                  map.addControl(control);
                });
              }

              map.setFitView();
            })
            .catch((e) => {
              console.error(e); //加载错误提示
            });
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);
