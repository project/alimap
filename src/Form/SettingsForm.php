<?php

namespace Drupal\alimap\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Alimap settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alimap_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['alimap.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('alimap.settings');

    // A Baidu Map API Key has exactly 24 or 32 alphanumeric characters.
    $form['alimap_api_key'] = [
      '#type' => 'textfield',
      '#title' => t('Alimap API Key'),
      '#required' => TRUE,
      '#description' => $this->t('to create alimap api key: <a href="@link" target="_blank">link</a>', [
        '@link' => 'https://console.amap.com/dev/key/app'
      ]),
      '#default_value' => $config->get('alimap_api_key'),
    ];
    $form['alimap_security_key'] = [
      '#type' => 'textfield',
      '#title' => t('Alimap  Security Key'),
      '#required' => false,
      '#default_value' => $config->get('alimap_security_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('alimap.settings')
      ->set('alimap_api_key', $form_state->getValue('alimap_api_key'))
      ->set('alimap_security_key', $form_state->getValue('alimap_security_key'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
