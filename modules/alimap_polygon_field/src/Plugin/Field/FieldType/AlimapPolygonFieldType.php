<?php
namespace Drupal\alimap_polygon_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of alimap_polygon_field.
 *
 * @FieldType(
 *   id = "alimap_polygon_field",
 *   label = @Translation("Alimap polygon field"),
 *   default_formatter = "alimap_polygon_field_default",
 *   default_widget = "alimap_polygon_field_default",
 * )
 */
class AlimapPolygonFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'type' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'geodata' => [
          'type' => 'text',
          'size' => 'medium',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['type'] = DataDefinition::create('string')
      ->setLabel(t('Type'));

    $properties['geodata'] = DataDefinition::create('string')
      ->setLabel(t('Geo data'));

    return $properties;
  }

}
